# http://minesweeperonline.com/

from sikuli import *

import random, time

from javax.swing import JOptionPane

def printBoard():
	for y in range(16):
		for x in range(30):
			if mineData[x][y] == 0:
				print ' ',
			else:
				print mineData[x][y],
		print ''

def load():
	for i in range(6):
		try:
			matches = mineArea.findAll(Pattern('%d.png' % i).similar(.95))
			while matches.hasNext():
				match = matches.next()
				x = (match.x-mineArea.x) / 16
				y = (match.y-mineArea.y) / 16 - 1
				mineData[x][y] = i
		except:
			pass


mineData = []
for x in range(30):
	mineData.append([])
	for y in range(16):
		mineData[x].append('-')

mineArea = Region(find("smiley.png"))
mineArea.setRect(
	mineArea.getX()-227,
	mineArea.getY()+23,
	480,
	300,
)

def areaChanged(event):
	load()
	printBoard()
	print "\n\n"

areaChanged(None)
mineArea.onChange(1, areaChanged)
mineArea.observe()
