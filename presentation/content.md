# Intelligent GUI Automation
Dominic Canare

![devICT](img/devict-logo.png)
![MakeICT](img/makeict-logo.png)



# Intelligent GUI Automation
## or "There's no API for that"
By combining computer vision (OpenCV) and coding (Jython (Java + Python)), Sikuli can be used to automate any graphical user interface with programmable intelligence. The vision module allows users programmatically identify visual elements, query the screen, and perform logical operations based on the results. For example, one could write a script to find all checkboxes on the screen and toggle their state if the first letter after the checkbox is a 'T'. This can be a powerful tool for GUI testing or automating UI's when an API is not available.



# Dominic Canare
* Twitter: @obi_dom
* Email: dom@makeict.org



# Automation
Wikipedia:
> Automation or automatic control, is the use of various control systems for operating equipment such as machinery, processes in factories, boilers and heat treating ovens, switching in telephone networks, steering and stabilization of ships, aircraft and other applications with minimal or reduced human intervention"


# Automation
Dom:
> Making stuff do stuff for you



# Levels of automation
* Level 0 - No automation. Maybe warnings.
* Level 1 - Presents pilot with options
* Level 2 - Asks pilot for confirmation
* Level 3 - Autopilot
* Level 4 - Skynet



# Automation
## Examples
* AutoHotkey
* AutoIt
* Selenium
<hr/>
* Sikuli



# Sikuli
## Intelligent automation
* Computer vision (OpenCV)<br/>+ Automation (Java Robot)
* Jython (2.7) (Java implementation of Python)
	* Code in python with access to J2SE API
	* Import and use Python modules as well
* WYSIWYS<small>cript</small> IDE
* OCR
* Works on ANY UI (web, native, VM, VNC)



# Video demos
* For work
	* https://www.youtube.com/watch?v=fJfGjdOkhrk
	* https://www.youtube.com/watch?v=FxDOlhysFcM&t=5m12s
* For fun
	* https://www.youtube.com/watch?v=A1poUsaXc44&t=15s
	* https://www.youtube.com/watch?v=zhhm09ECcC8&t=15s
* Test automation
* Sikuli Slides
	* Code-free scripts
	* https://www.youtube.com/watch?v=22yNhSgplDg&t=4m55s



# Live demo
* Wordpress
* Minesweeper
* "Dr." Mario



# Cheat sheet
    find(), findAll()
    
    wait(), exists(), waitVanish()
    
    similar(%) and exact()
    
    capture()
    
    observe() / stopObserver()
        onAppear(), onVanish(), onChange()
        
    grow(), nearby(), above(), below(), left(), right()
    
    click(), doubleClick(), rightClick(), type(), hover(), drag/dropAt()
        type(Key.ENTER(), KeyModifier.SHIFT)()
        
    text() and paste()



# Tips
* Make idempotent changes
* Add comments to your sleeps
* Keyboard navigation vs mouse navigation for dropdowns
* exists() followed by getLastMatch()
* ALT+SHIFT+c to kill (IDE only)
* `sikuli -i` for interactive, `execfile()` to load script
* Use the App class to find your program



# Challenge
* Let's see who can build the BEST bot!
* https://bitbucket.org/dcanare/intelligent-gui-automation/src
